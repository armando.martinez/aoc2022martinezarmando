with open('./text/2022-12-01-input.txt') as f:
    lines = f.readlines()
max_elf1 = 0
current_elf = 0
for i in lines:
    if i == "\n":
        if current_elf > max_elf1:
            max_elf1 = current_elf
        current_elf = 0
    else:
        current_elf += int(i[0:-1])
print(max_elf1)