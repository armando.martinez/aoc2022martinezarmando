def rockpaperscissors(a, b):
    if b == 'X': 
        if a == 'A':
            cheatedpoints = 3
            # print("rock scissors")
        elif a == 'B':
            cheatedpoints = 1
            # print("paper rock")
        elif a == 'C':
            cheatedpoints = 2
            # print("scissors paper")
    elif b == 'Y':
        if a == 'A':
            cheatedpoints = 1
            # print("rock rock")
        elif a == 'B':
            cheatedpoints = 2
            # print("paper paper")
        elif a == 'C':
            cheatedpoints = 3
            # print("scissors scissors")
    elif b == 'Z':
        if a == 'A':
            cheatedpoints = 2
            # print("rock paper")
        elif a == 'B':
            cheatedpoints = 3
            # print("paper scissors")
        elif a == 'C':
            cheatedpoints = 1
            # print("scissors rock")
    return cheatedpoints

with open('./text/2022-12-02-input.txt') as f:
    lines = f.readlines()

b_points = 0
for i in lines:
    a = i[0]
    b = i[2]
    if b == 'Y':
        b_points += 3
    elif b == 'Z':
        b_points += 6
    b_points += rockpaperscissors(a, b)
print (b_points)
