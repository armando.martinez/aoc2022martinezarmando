with open('./text/2022-12-03-input.txt') as f:
    lines = f.readlines()

sumatori = 0
for i in lines:
    half = len(i) // 2
    halfi = i[half:-1]
    # print(halfi)
    for val in i:
        if val in halfi:
            if ord(val) >= 65 and ord(val)<= 90:
                sumatori += (ord(val) - 64 + 26)
                # print(ord(val) - 64 + 26)
            elif ord(val) >= 97 and ord(val) <= 122:
                sumatori += (ord(val) - 96)
                # print(ord(val) - 96)
            break
print(sumatori)