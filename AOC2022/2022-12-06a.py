with open('./text/2022-12-06-input.txt') as f:
    lines = f.readlines()

signal = lines[0]

for i in range(len(signal)):
    if i < len(signal) - 4:
        if signal[i] not in [signal[i + 1], signal[i + 2], signal[i + 3]] and signal[i + 1] not in [signal[i + 2], signal[i + 3]] and signal[i + 2]!= signal[i + 3]:
            marker = i + 4
            break
print(marker)