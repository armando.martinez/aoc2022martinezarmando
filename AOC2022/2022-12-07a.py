import os

def create_dirtree (code):
    path = "./"
    for i in range(len(code)):
        line = code[i]
        if line[0] == "$":
            if line[0:4] == "$ cd" and line[0:6] != "$ cd /": # ESCRIURE EL PATH EN BASE ALS CD
                if len(path) > 2:
                    if line[-3:-1] == "..":
                        path = path[:path.rfind('/')]
                        if len(path) < 2:
                            path = "."
                    else:
                        path = path + "/" + line[5: -1]
                else:
                    if line[-3:-1] == "..":
                        path = "."
                    else:
                        path = "./" + line[5: -1]
        elif line[0:3] == "dir": # CREATE DOCUMENT AT PATH
            fullpath = str("./2022-12-07-output/" + str(path) + "/" + line[4: -1])
            os.mkdir(fullpath)
        else:
            size = line[:line.find(" ")]
            name = line[line.find(" ") + 1: -1]
            os.system(str("echo " + size + " " + path + " > ./2022-12-07-output/" + str(path) + "/" + name))


with open('./text/2022-12-07-input.txt') as f:
    lines = f.readlines()

create_dirtree(lines)
os.system("find ./2022-12-07-output/* -type f -exec cat {} + > ./2022-12-07-output/LIST.TXT")
f.close()

with open('./2022-12-07-output/LIST.TXT') as f2:
    lines = f2.readlines()

all_sizes = {}
for line in lines: # CREATE DICTIONARY
    entry = line[line.find(" ") + 1: -1]
    all_sizes[entry] = 0

for line in lines:
    size = line[:line.find(" ")]
    entry = line[line.find(" ") + 1: -1]
    if entry in all_sizes:
        all_sizes[entry] += int(size)
        while len(entry) >= 3:
            if entry[:entry.rfind("/")] in all_sizes:
                entry = entry[:entry.rfind("/")]
                all_sizes[entry] += int(size)
result = 0
for val in all_sizes:
    if all_sizes.get(val) < 100000:
        result += all_sizes.get(val)
print(all_sizes)
print(result)
input()
os.system("rm -rf ./2022-12-07-output/*")