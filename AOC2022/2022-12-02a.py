def rockpaperscissors(a, b):
    if a == b:
        winner = 'c'
    elif (a == 'A' and b == 'C') or (a == 'B' and b == 'A') or (a == 'C' and b == 'B'):
        winner = 'a'
    elif (b == 'A' and a == 'C') or (b == 'B' and a == 'A') or (b == 'C' and a == 'B'):
        winner = 'b'
    return winner

with open('./text/2022-12-02-input.txt') as f:
    lines = f.readlines()

b_points = 0
for i in lines:
    a = i[0]
    b = i[2]
    if b == 'X':
        b_points += 1
        b = 'A'
    if b == 'Y':
        b_points += 2
        b = 'B'
    if b == 'Z':
        b_points += 3
        b = 'C'
    winner = rockpaperscissors(a, b)
    if winner == 'b':
        b_points += 6
    else:
        b_points += 3
print (b_points)
