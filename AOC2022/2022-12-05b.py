with open('./text/2022-12-05-input.txt') as f:
    lines = f.readlines()

cargo_crane = ["", "", "", "", "", "", "", "", ""]
countingCranes = True
for line in lines:
    #print(line[0:])
    while countingCranes == True:
        if line[1].isalpha():
            cargo_crane[0] = line[1] + cargo_crane[0]
        if line[5].isalpha():
            cargo_crane[1] = line[5] + cargo_crane[1]
        if line[9].isalpha():
            cargo_crane[2] = line[9] + cargo_crane[2]
        if line[13].isalpha():
            cargo_crane[3] = line[13] + cargo_crane[3]
        if line[17].isalpha():
            cargo_crane[4] = line[17] + cargo_crane[4]
        if line[21].isalpha():
            cargo_crane[5] = line[21] + cargo_crane[5]
        if line[25].isalpha():
            cargo_crane[6] = line[25] + cargo_crane[6]
        if line[29].isalpha():
            cargo_crane[7] = line[29] + cargo_crane[7]
        if line[33].isalpha():
            cargo_crane[8] = line[33] + cargo_crane[8]
        #
        if line[1].isnumeric():
            countingCranes = False
        else: break
    if countingCranes == False and line[0] == "m":
        move = int(line[5:line.find("f")-1])
        origin = int(line[line.find("from")+5: line.find("t")-1]) - 1
        destination = int(line[line.find("to")+3:]) - 1
        #print(move, origin, destination)
        cargo_crane[destination] = cargo_crane[destination] + (cargo_crane[origin][-move:])
        cargo_crane[origin] = cargo_crane[origin][:-move]
        print("dest: " + cargo_crane[destination])
        print("origin: " + cargo_crane[origin]) 
print(cargo_crane)