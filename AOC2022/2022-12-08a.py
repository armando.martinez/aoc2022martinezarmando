def count_trees(forest):
    count = 0
    for row in forest:
        for tree in row:
            if tree.isalnum():
                count += 1
    return count

### MAIN PROGRAM

with open('./text/2022-12-08-test.txt') as f:
    lines = f.readlines()

trees = count_trees(lines)
rows = len(lines)
columns = len(lines[0]) - 1
visible_trees = 0
print(lines[0:2])

for row in range(rows): # for each row
    for col in range(columns): # for each column
        if row != 0 and col != 0 and row != rows and col != columns: # exception: exterior trees
            for tree_row in lines[0:row]: # check top trees
                if lines[row][col] > tree_row[col]:
                    visible = True
                    print(lines[row][col], tree_row[col])
            if visible:
                visible_trees += 1

print(trees, rows, columns, visible_trees)
