with open('./text/2022-12-03-input.txt') as f:
    lines = f.readlines()

suma = 0
for i in range(1, len(lines) + 1):
    if (i) % 3 == 0:
        for val in lines[i - 1]:
            if val in lines[i- 2] and val in lines[i - 3]:
                # print(val)
                if ord(val) >= 65 and ord(val)<= 90:
                    suma += (ord(val) - 64 + 26)
                    # print(ord(val) - 64 + 26)
                elif ord(val) >= 97 and ord(val) <= 122:
                    suma += (ord(val) - 96)
                    # print(ord(val) - 96)
                break
print(suma)