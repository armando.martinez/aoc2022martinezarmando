with open('./text/2022-12-04-input.txt') as f:
    lines = f.readlines()

count = 0
for line in lines:
    range1a = line[0:line.find("-")]
    range1b = line[line.find("-") + 1: line.find(",")]
    subline = line[line.find(",") + 1:]
    range2a = subline[0: subline.find("-")]
    range2b = subline[subline.find("-") + 1:]
    range1 = range(int(range1a), int(range1b) + 1)
    range2 = range(int(range2a), int(range2b) + 1)
    if int(range2a) in range1 and int(range2b) in range1:
        count += 1
    elif int(range1a) in range2 and int(range1b) in range2:
        count += 1

print(count)