with open('./text/2022-12-06-input.txt') as f:
    lines = f.readlines()

signal = lines[0]

for i in range(len(signal)):
    if i < len(signal) - 14:
        isMarker = True
        for j in range(i, i + 14):
            toCompare = [signal[i], signal[i + 1], signal[i + 2], signal[i + 3], signal[i + 4], signal[i + 5], signal[i + 6], signal[i + 7], signal[i + 8], signal[i + 9], signal[i + 10], signal[i + 11], signal[i + 12], signal[i + 13]]
            if toCompare.count(signal[j]) > 1:
                isMarker = False
        if isMarker:
            marker = i + 14
            break
print(marker)